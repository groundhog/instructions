# Openstack setup
If you want to manage your own private cloud and launch VMs, you can rely on openstack which should provide a relatively easy start.
Below are the notes to instlling Openstack on one or more Ubuntu 20.04 hosts:

### Install openstack to the controller node:
- Clone the `Automation` project and run the `openstack/install_openstack.sh` script, then run
```
$ sudo microstack init --auto --control
```
Finally, test the deployment by running:
```
$ microstack launch cirros --name test
```

### Prepare for launching VMs
- To add Ubuntu's cloud image to openstack: run the `create_ubuntu_cloud_img.sh` script
- You can use our pre-prepared image with all dependencies by running the `create_ubuntu_groundhog_img.sh` script which downloads the image from https://groundhog.mpi-sws.org/downloads/groundhog-ow-ubuntu20.qcow2 and uploads it to the OpenStack deployment. 
- Create VM flavors for Groundhog: run the `create_gh_flavors.sh` script
- Create floating IPs to allow connecting to VMs from the host: run the `create_floating_ips.sh` script

### Access OpenStack Web interface
You can forward the OpenStack Horizon web interface ports as follows:
```
$ ssh -L9000:10.20.20.1:80 $UNAME@$SERVER
```
From there you can manually launch instances (http://localhost:9000/project/instances/) and assign availability zone (AZ) names (http://localhost:9000/admin/aggregates/)

### Launch VMs for an experiment
- Create a router: run `./create_router.sh 1`
- Create a network to allow the VMs to communicate: run `./create_network_with_name.sh <ID>`
- Create a latency or xput VM group by running the scripts `create_latency_group.sh <ID> <AZ> <IMG>` or `create_xput_group.sh <ID> <AZ> <AZC> <IMG>`

`<ID>` is the VM group ID and should be a natural number. The VMs created will have the format ow-core-<ID>, ow-invoker0-<ID>, and for throughput groups ow-client-<ID>.

`<AZ>` is the availability zone name you set using the OpenStack Horizon web interface, and this availability zone will be used to launch the ow-core-<ID> and ow-invoker0-<ID> VMs.

`<AZC>` is the availability zone name you set using the OpenStack Horizon web interface, and this availability zone will be used to launch the ow-client-<ID> VM.

`<IMG>` defaults to `groundhog-ow-ubuntu20` which is created using the `create_ubuntu_groundhog_img.sh`



