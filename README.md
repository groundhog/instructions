# Groundhog Artifact Evaluation Guide
Thank you for taking the time to evaluate Groundhog! We appereciate your service to the community, and we hope the evaluation guide will be enjoyable to follow and will optimize the use of your time!

## Description of the projects in https://gitlab.mpi-sws.org/groundhog:
- Project `Groundhog` contains the source code and functionality tests of Groundhog
- Project `OpenWhisk` contains OpenWhisk source code with the following modifications:
    - Updates dependencies to run on Ubuntu 20.04 instead of 18.04
    - Updated deployment configurations to enable the 2-VM distributed deployment
    - Updated deployment configurations to enable functions to execute more than 1 minute
- Project `openwhisk-runtime-python` is OpenWhisk's python runtime with the following changes:
    - Basline (insecure) have additional instrumentation to measure the time taken by the function's handler (from inside the container)
    - Groundhog enables optional groundhog support
    - Fork implements fork-based request isolation (possible for python because the python runtime, and the functions evaluated are all single-threaded)
- Project `openwhisk-runtime-node` is OpenWhisk's NodeJS runtime with the following changes:
    - Basline (insecure) have additional instrumentation to measure the time taken by the function's handler (from inside the container)
    - Baseline-refactored (insecure) same as baseline but refactored to follow the proxy design
    - Groundhog enables optional groundhog support
    - Groundhog-no-proxy enables optional groundhog support for the single-process original runtime design
- Project `openwhisk-runtime-C` is our implementaion of a C runtime for OpenWhisk to support running C functions, it has the following configutations:
    - Basline (insecure) 
    - Groundhog enables optional groundhog support
    - Fork implements fork-based request isolation (possible for C because all our C functions are single-threaded)
- Project `Automation ` have all the scripts required for automating the installation (when possible), running experiments, and plotting results.

## This artifact evaluation guide has the following main sections:
1) Steps to run Groundhog standalone on a standard linux kernel
2) Steps to deploy OpenWhisk with the Groundhog enabled runtimes
3) Steps to reproduce the results

### Software requirements
- Linux Operating system. Kernel v3.11+ [^1]. We recommend using Ubuntu 20.04.

### 1) Steps to run Groundhog standalone on a standard linux kernel
Assuming you have a fresh Ubuntu 20.04 virtual machine.
```
$ mkdir groundhog-evaluation; cd groundhog-evaluation; git clone https://gitlab.mpi-sws.org/groundhog/groundhog.git; cd groundhog
$ ./install_dependencies.sh #sudo apt-get install -y build-essential libglib2.0-dev
$ make test
```

### 2) Steps to deploy OpenWhisk with the Groundhog enabled runtimes
- If you need to install openstack to be able to mange your own VMs, please follow the guide in `openstack-setup-notes.md`
- You can use our VM image with all dependencies at: https://groundhog.mpi-sws.org/downloads/groundhog-ow-ubuntu20.qcow2
- You can prepare a new VM image based on Ubuntu 20.04 cloud image (https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img) by running the script `prepare_vm.sh` (from https://gitlab.mpi-sws.org/groundhog/automation -> prepare-vm) inside it.
- Once the image is ready, make a snapshot of it and use the snapshot to launch 2 VMs (4 cores, 64 GBs of RAM, 100GBs of Disk each) with the names ow-core-X, and ow-invoker0-X where X is a number, and ensure that the VMs are reachable through the assigned names. Alternatively, modify the VMs name in all scripts in the repo (https://gitlab.mpi-sws.org/groundhog/automation/) to match the chosen name/IP.
- For throughput experiments, launch an additional client VM (2 cores, 16 GBs of RAM, 50GBs of disk should be enough).
- Automatic scripts for creating, launching the VMs for OpenStack can be found in `openstack-setup-notes.md`

### 3) Steps to reproduce the results
After the experiment setup is ready (VM groups are created and running), we need an additional controller node/vm/server (which can use the same VM image available at: https://groundhog.mpi-sws.org/downloads/groundhog-ow-ubuntu20.qcow2) that can communicate with the OpenWhisk core VM named ow-core-X. On this controller node, experiments will be started and data will be automatically retrieved. Please make sure the VM ssh keys are in ~/.ssh/id_groundhog on the controller node:
```
cp /local/workspace/automation/prepare-vm/id_groundhog* ~/.ssh
cat /local/workspace/automation/prepare-vm/id_groundhog.pub >> ~/.ssh/authorized_keys
sudo chmod -R 600 ~/.ssh
sudo chmod 700 ~/.ssh
```
Run the corresponding experiment script from /local/workspace/automation/experiments. Below is the expected duration for the down-sized experiments. 
|     | Scripts to run                                                                                                                                                                                                                 | Time   |            | Corresponding Figure | \#Configurations |
| :-- | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :----- | :--------- | :------------------- | :--------------- |
|     |                                                                                                                                                                                                                                | Human  | Compute    |                      |                  |
| E1  | run\_latency\_microbenchmark\_vary\_dirtied\_fast.sh <br> run\_latency\_microbenchmark\_vary\_dirtied\_slow.sh <br> run\_latency\_microbenchmark\_vary\_pages\_fast.sh <br> run\_latency\_microbenchmark\_vary\_pages\_slow.sh | 5 mins | ~4 hours   | Fig 3                | 120              |
| E2  | run\_latency\_python.sh <br> run\_latency\_nodejs.sh <br> run\_latency\_pyperf.sh <br> run\_latency\_polybench\_long.sh \# > 10s per request <br> run\_latency\_polybench\_short.sh \# < 10s per request                       | 5 mins | ~10 hours  | Fig 4                | 232              |
| E3  | run\_xput\_python.sh <br> run\_xput\_nodejs.sh <br> run\_xput\_pyperf.sh <br> run\_xput\_polybench.sh                                                                                                                          | 5 mins | ~100 hours | Fig 5                | 232              |
| E4  | run\_scalability\_1core.sh <br> run\_scalability\_2core.sh <br> run\_scalability\_3core.sh <br> run\_scalability\_4core.sh                                                                                                     | 5 mins | ~50 hours  | Fig 7                | 144              |

### Plotting paper graphs
Full-length paper data can be found at (https://groundhog.mpi-sws.org/downloads/DATA-EUROSYS23.tgz), and can be plotted using the notebook in the repository `Automation` -> plot.
In the top section, update the path to the data source and run the notebook.

[^1]: We identified and reported a bug that affected the accuracy of the SD-Bits memory tracking in v5.6, it was fixed in
v5.12 (details and a buggy kernel detector [here](https://lore.kernel.org/linux-mm/daa3dd43-1c1d-e035-58ea-994796df4660@suse.cz/T/))
